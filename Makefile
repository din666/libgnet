all:
	rm -rf build
	mkdir -p build
	(cd build && cmake -DCMAKE_BUILD_TYPE=Debug -DEXTRALOGS=1 -DBUILD_SHARED_LIBS=1 .. && make)
