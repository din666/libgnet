
#include "stdafx.h"

#ifdef EXTRALOGS
#include <fstream>
#endif

#ifdef __linux__
#include <stdlib.h>
#include "local_defs.h"
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <errno.h>
#include <iconv.h>
#endif

#include <time.h>
//#include <atltime.h>
#include <stdio.h>
#include <wchar.h>
#include "Gnet.h"
#include <string>

#define TIME_LEN 34

#define BASE_ERRNO     7

static const wchar_t *g_MethodNames[] = {L"CheckKey"};

static const wchar_t *g_MethodNamesRu[] = {L"�������������"};

static const wchar_t g_kClassNames[] = L"CGNET";
static IAddInDefBase *pAsyncEvent = NULL;

uint32_t convToShortWchar(WCHAR_T** Dest, const wchar_t* Source, uint32_t len = 0, const std::string & SourceEncoding = "UTF-8");
uint32_t convFromShortWchar(wchar_t** Dest, const WCHAR_T* Source, uint32_t len = 0);
uint32_t getLenShortWcharStr(const WCHAR_T* Source);

// Definitions
#define CryptPU     0x8566783lu
#define CryptRD     0x17d49c84u
#define CryptWR     0x9843de24u
#define CryptMS     0x47832845u


#define GrdDC_Public        0x000000000u - CryptPU
#define GrdDC_PrivateRead   0x000000000u - CryptRD
#define GrdDC_PrivateWrite  0x000000000u - CryptWR
#define GrdDC_PrivateMaster 0x00000000u - CryptMS

#define GrdDC_PublicSign        0x000000000u - CryptPU
#define GrdDC_PrivateReadSign   0x000000000u - CryptRD
#define GrdDC_PrivateWriteSign  0x000000000u - CryptWR
#define GrdDC_PrivateMasterSign 0x000000000u - CryptMS

#ifdef EXTRALOGS
static void ExtraLog(const std::string & str) {
    static std::ofstream ofs("/tmp/GNET.log", std::ios_base::app);
    ofs << str << std::endl;
    ofs.flush();
}
#endif

//---------------------------------------------------------------------------//
long GetClassObject(const WCHAR_T* wsName, IComponentBase** pInterface)
{
    if(!*pInterface)
    {
        *pInterface= new CGNET;
        return (long)*pInterface;
    }
    return 0;
}
//---------------------------------------------------------------------------//
long DestroyObject(IComponentBase** pIntf)
{
    if(!*pIntf)
        return -1;

    delete *pIntf;
    *pIntf = 0;
    return 0;
}
//---------------------------------------------------------------------------//
const WCHAR_T* GetClassNames()
{
    static WcharWrapper s_names(g_kClassNames);
    return s_names;
}
//---------------------------------------------------------------------------//

// CGNET
//---------------------------------------------------------------------------//
CGNET::CGNET() :
      m_iConnect(NULL)
    , m_iMemory(NULL)
    , m_intProgNumber(0)
    , Status(0)
    , m_outhnd(0)
    , GrdDongle(NULL)
    , LastDateCheck(CTime(boost::gregorian::date(1980,1,1)))
{
#ifdef EXTRALOGS
    ExtraLog("CGNET::CGNET");
#endif
}

//---------------------------------------------------------------------------//
CGNET::~CGNET()
{
}
//---------------------------------------------------------------------------//
bool CGNET::Init(void* pConnection)
{
    m_iConnect = (IAddInDefBase*)pConnection;

    return m_iConnect != NULL;
}
//---------------------------------------------------------------------------//
long CGNET::GetInfo()
{
    // Component should put supported component technology version
    // This component supports 2.0 version
    return 2000;
}
//---------------------------------------------------------------------------//
void CGNET::Done()
{
    if(GrdDongle) {
        GrdDongle->Logout();
        delete GrdDongle; //
        GrdDongle = 0;
    }
}
/////////////////////////////////////////////////////////////////////////////
// ILanguageExtenderBase
//---------------------------------------------------------------------------//
bool CGNET::RegisterExtensionAs(WCHAR_T** wsExtensionName)
{

    if (m_iMemory)
    {
        const wchar_t *wsExtension = L"YUVERS";
        uint32_t iActualSize = ::wcslen(wsExtension) + 1;
        if(m_iMemory->AllocMemory((void**)wsExtensionName, iActualSize * sizeof(WCHAR_T))) {
            ::convToShortWchar(wsExtensionName, wsExtension, iActualSize);
        }
        return true;
    }

    return false;
}
//---------------------------------------------------------------------------//
long CGNET::GetNProps()
{
    // You may delete next lines and add your own implementation code here
    return ePropLast;
}
//---------------------------------------------------------------------------//
long CGNET::FindProp(const WCHAR_T* wsPropName)
{
    long plPropNum = -1;

    return plPropNum;
}
//---------------------------------------------------------------------------//
const WCHAR_T* CGNET::GetPropName(long lPropNum, long lPropAlias)
{
    return NULL;

}
//---------------------------------------------------------------------------//
bool CGNET::GetPropVal(const long lPropNum, tVariant* pvarPropVal)
{
    switch(lPropNum)
    {
        default:
            return false;
    }

    return true;
}
//---------------------------------------------------------------------------//
bool CGNET::SetPropVal(const long lPropNum, tVariant *varPropVal)
{
    switch(lPropNum)
    {
        default:
            return false;
    }

    return true;
}
//---------------------------------------------------------------------------//
bool CGNET::IsPropReadable(const long lPropNum)
{
    switch(lPropNum)
    {
        default:
            return false;
    }

    return false;
}
//---------------------------------------------------------------------------//
bool CGNET::IsPropWritable(const long lPropNum)
{
    switch(lPropNum)
    {
        default:
            return false;
    }

    return false;
}
//---------------------------------------------------------------------------//
long CGNET::GetNMethods()
{
    return eMethLast;
}
//---------------------------------------------------------------------------//
long CGNET::FindMethod(const WCHAR_T* wsMethodName)
{
    long plMethodNum = -1;
    wchar_t* name = NULL;

    ::convFromShortWchar(&name, wsMethodName);

    plMethodNum = findName(g_MethodNames, name, static_cast<const uint32_t>(eMethLast));

    if (plMethodNum == -1)
        plMethodNum = findName(g_MethodNamesRu, name, eMethLast);

    if ( NULL != name ) {
        delete[] name;
    }
    return plMethodNum;
}
//---------------------------------------------------------------------------//
const WCHAR_T* CGNET::GetMethodName(const long lMethodNum, const long lMethodAlias)
{
    if (lMethodNum >= eMethLast)
        return NULL;

    const wchar_t *wsCurrentName = NULL;
    WCHAR_T *wsMethodName = NULL;
    int iActualSize = 0;

    switch(lMethodAlias)
    {
        case 0: // First language
            wsCurrentName = (wchar_t*)g_MethodNames[lMethodNum];
            break;
        case 1: // Second language
            wsCurrentName = (wchar_t*)g_MethodNamesRu[lMethodNum];
            break;
        default:
            return 0;
    }

    iActualSize = wcslen(wsCurrentName)+1;

    if (m_iMemory && wsCurrentName)
    {
        if(m_iMemory->AllocMemory((void**)&wsMethodName, iActualSize * sizeof(WCHAR_T)))
            ::convToShortWchar(&wsMethodName, wsCurrentName, iActualSize);
    }

    return wsMethodName;
}
//---------------------------------------------------------------------------//
long CGNET::GetNParams(const long lMethodNum)
{
    switch(lMethodNum)
    {
        case eMethLogin:
            return 6;
        default:
            return 0;
    }

    return 0;
}
//---------------------------------------------------------------------------//
bool CGNET::GetParamDefValue(const long lMethodNum, const long lParamNum,
        tVariant *pvarParamDefValue)
{
    TV_VT(pvarParamDefValue)= VTYPE_EMPTY;

    switch(lMethodNum)
    {
        default:
            return false;
    }

    return false;
}
//---------------------------------------------------------------------------//
bool CGNET::HasRetVal(const long lMethodNum)
{
    switch(lMethodNum)
    {
        case eMethLogin:
            return true;
        default:
            return false;
    }

    return false;
}
//---------------------------------------------------------------------------//
bool CGNET::CallAsProc(const long lMethodNum,
        tVariant* paParams, const long lSizeArray)
{
    switch(lMethodNum)
    {
        default:
            return false;
    }

    return true;
}
//---------------------------------------------------------------------------//
bool CGNET::CallAsFunc(const long lMethodNum,
        tVariant* pvarRetValue, tVariant* paParams, const long lSizeArray)
{
    bool ret	= false;
    int Yes		= 1001;     // ������������� ���������
    int No		= 110;      // ������������� ���������
    int Result	= No;       // �� ��������� ��������� �������� �������������
    int Key		= 0;        // ���� ���������� (���������� ����� �������� �������)

    bool SoftKey	= false;// ���������� ����
    bool localKey	= true;	// ��������� ����
    bool takeLogin	= true; // �������� ��������

    char	*name	= 0;		// ���� ��������
    wchar_t	*wsTmp	= 0;
    int		 size	= 0;
    char	*mbstr	= 0;

    switch(lMethodNum)
    {
        case eMethLogin:

            int ProgNumber;  // ����� ��������� (���������� ����� �������� �������)
            //  0 - ����� ���������   ( 0 - ���, 1 - ��� � �.�.)
            //  1 - ���� ����������   ( ��������� �����)
            //  2 - ��������� ����	  (������ - �� , ���� - ���)
            //  3 - �������� �������� (������ - �� , ���� - ���)
            //  4 - ��� ����� ��������
            //  5 - ��� �����		  ( 0 - ��������, 1 - ����������)

            if (lSizeArray != 6 ) {    // ���� �������� �� ��� ���������, �� ������
                return false;
            };

            switch(TV_VT(&paParams[0]))
            {
                case VTYPE_I4:
                    ProgNumber = TV_I4(&paParams[0]);  // �������� ������ �������� ��������������
                    break;
                default:
                    return false;
            }

            switch(TV_VT(&paParams[1]))
            {
                case VTYPE_I4:
                    Key = TV_I4(&paParams[1]); // �������� ������ �������� ��������������
                    break;
                default:
                    return false;
            }

            switch(TV_VT(&paParams[2]))
            {
                case VTYPE_BOOL:
                    localKey = TV_BOOL(&paParams[2]); // �������� �������� ��������� ����
                    break;
                default:
                    return false;
            }

            switch(TV_VT(&paParams[3]))
            {
                case VTYPE_BOOL:
                    takeLogin = TV_BOOL(&paParams[3]); // �������� �������� ������ �����
                    break;
                default:
                    return false;
            }

            switch(TV_VT(&paParams[4]))
            {
                case VTYPE_PSTR:
                    name = paParams[4].pstrVal;  // �������� ���� � ����� � �����������
                    break;
                case VTYPE_PWSTR:
                    ::convFromShortWchar(&wsTmp, TV_WSTR(&paParams[4]));
                    size = wcstombs(0, wsTmp, 0)+1;
                    mbstr = new char[size];
                    memset(mbstr, 0, size);
                    size = wcstombs(mbstr, wsTmp, getLenShortWcharStr(TV_WSTR(&paParams[4])));
                    name = mbstr;
                    break;
                default:
                    return false;
            }

            switch(TV_VT(&paParams[5]))
            {
                case VTYPE_BOOL:
                    SoftKey = TV_BOOL(&paParams[5]); // �������� ������ �������� ��������� ����
                    break;
                default:
                    return false;
            }

		 if(ProgNumber != m_intProgNumber) {
			Status		= 0;
			m_outhnd    = 0;
		}
		else {
			if(Status) {                                           // ���� ���� ��� ������ � ���������� ���, ��
                CTime CurrDate = boost::posix_time::microsec_clock::local_time();
				CTimeSpan DeltaTime1 = CurrDate - LastDateCheck;

                namespace pt = boost::posix_time;

                CTime t1( boost::gregorian::date(2011, 4, 20),pt::hours(22) + pt::minutes(0) + pt::seconds(0));
                CTime t2( boost::gregorian::date(2011, 4, 20),pt::hours(22) + pt::minutes(30) + pt::seconds(0));
				// �������� ��������
				CTimeSpan DeltaTime2 = t2 - t1; 

                int Delta1 = int(DeltaTime1.seconds());
                int Delta2 = int(DeltaTime2.seconds());

				if(Delta1 <= Delta2) {                             // ��������� ������������� �������� ����� �� ��������� ������� (30 ���. �� ���������)
					Result += Yes;
					Result ^= Key;
					TV_VT(pvarRetValue)		= VTYPE_I4;
					TV_I4(pvarRetValue)		= Result;			   // ���� ������ � ��������, �� ���������� ������������� ��������� ��������	
					return true;
				}
			}

		};

		m_intProgNumber = (SoftKey ?  100 : ProgNumber);//ProgNumber;

		int nRet = 0;

		HANDLE m_hGrd = NULL;

		if(m_outhnd) {
			GrdCleanup();
			m_outhnd = 0;
        }

		if(!m_outhnd) { // ���� ��������� �� ������ ������ � ������ �� �����, ��
			nRet = GrdStartupEx(localKey ? GrdFMR_Local : GrdFMR_Remote, localKey ? NULL : name, 0);
			if (nRet == 60) {
				GrdCleanup();
				nRet = GrdStartupEx(localKey ? GrdFMR_Local : GrdFMR_Remote, localKey ? NULL : name, 0);
			};
			m_hGrd = GrdCreateHandle(m_hGrd, GrdCHM_MultiThread, NULL);
			m_outhnd = m_hGrd;
			nRet = GrdSetAccessCodes(m_hGrd, GrdDC_PublicSign+CryptPU, GrdDC_PrivateReadSign+CryptRD, 0, 0);
			nRet = GrdSetWorkMode(m_hGrd, GrdWM_UAM, GrdWMFM_DriverAuto);
			nRet = GrdSetFindMode (m_hGrd, localKey ? GrdFMR_Local : GrdFMR_Remote, GrdFM_NProg, m_intProgNumber,0,0,0,16,GrdDT_GSII64,GrdFMM_ALL,SoftKey ?  GrdFMI_SP : GrdFMI_USB);
			nRet = GrdLogin(m_hGrd, -1l, GrdLM_PerHandle);//GrdLM_PerProcess);//GrdLM_PerStation);
        } else {
			m_hGrd = m_outhnd;
		}

		if((takeLogin == 0) && (nRet == 0)) {
			nRet	= GrdLogout(m_hGrd, 0);
			//GrdDongle->Logout();											 // ��������� ���� (�� ��������� ���������� ������� �����)
		};
		Status	= !nRet;

		if(Status) {                                                         // ���� ���� ��� ������� ��������, ��
			LastDateCheck	= boost::posix_time::microsec_clock::local_time(); //CTime::GetCurrentTime();                       // ���������� ����� ��������
			Result			+= Yes;
		};

		Result ^= Key;                                                       // ������ ��������� �������� � ���������� ������ ����������

		TV_VT(pvarRetValue)		= VTYPE_I4;
		TV_I4(pvarRetValue)		= Result;                                    // � ���������� ���������

		ret = true;
		break;

    }
    return ret; 
}
//---------------------------------------------------------------------------//
void CGNET::SetLocale(const WCHAR_T* loc)
{
#ifndef __linux__
    _wsetlocale(LC_ALL, loc);
#else
    //We convert in char* char_locale
    //also we establish locale
    //setlocale(LC_ALL, char_locale);
#endif
}
/////////////////////////////////////////////////////////////////////////////
// LocaleBase
//---------------------------------------------------------------------------//
bool CGNET::setMemManager(void* mem)
{
    m_iMemory = (IMemoryManager*)mem;
    return m_iMemory != 0;
}
//---------------------------------------------------------------------------//
void CGNET::addError(uint32_t wcode, const wchar_t* source, 
                        const wchar_t* descriptor, long code)
{
    if (m_iConnect)
    {
        WCHAR_T *err = 0;
        WCHAR_T *descr = 0;

        ::convToShortWchar(&err, source);
        ::convToShortWchar(&descr, descriptor);

        m_iConnect->AddError(wcode, err, descr, code);
        delete[] err;
        delete[] descr;
    }
}
//---------------------------------------------------------------------------//
long CGNET::findName(const wchar_t* names[], const wchar_t* name, 
                        const uint32_t size) const
{
    long ret = -1;
    for (uint32_t i = 0; i < size; i++)
    {
        if (!wcscmp(names[i], name))
        {
            ret = i;
            break;
        }
    }
    return ret;
}
//---------------------------------------------------------------------------//
uint32_t convToShortWchar(WCHAR_T** Dest, const wchar_t* Source, uint32_t len, const std::string & SourceEncoding)
{
    if (!len)
        len = ::wcslen(Source) + 1;

    if (!*Dest)
        *Dest = new WCHAR_T[len];

    WCHAR_T* tmpShort = *Dest;
    wchar_t* tmpWChar = (wchar_t*) Source;
    uint32_t res = 0;

    ::memset(*Dest, 0, len * sizeof(WCHAR_T));
#ifdef __linux__
    size_t succeed = (size_t)-1;
    size_t f = len * sizeof(wchar_t), t = len * sizeof(WCHAR_T);
    const char* fromCode = sizeof(wchar_t) == 2 ? "UTF-16" : "UTF-32";
    iconv_t cd = iconv_open(SourceEncoding.c_str(), fromCode);
    if (cd != (iconv_t)-1)
    {
        succeed = iconv(cd, (char**)&tmpWChar, &f, (char**)&tmpShort, &t); 
        iconv_close(cd);
        if(succeed != (size_t)-1)
            return (uint32_t)succeed;
	ExtraLog(std::string("invalid decode from:") + SourceEncoding); // HZ
    }
#endif //__linux__
    for (; len; --len, ++res, ++tmpWChar, ++tmpShort)
    {
        *tmpShort = (WCHAR_T)*tmpWChar;
    }

    return res;
}
//---------------------------------------------------------------------------//
uint32_t convFromShortWchar(wchar_t** Dest, const WCHAR_T* Source, uint32_t len)
{
    if (!len)
        len = getLenShortWcharStr(Source) + 1;

    if (!*Dest)
        *Dest = new wchar_t[len];

    wchar_t* tmpWChar = *Dest;
    WCHAR_T* tmpShort = (WCHAR_T*)Source;
    uint32_t res = 0;

    ::memset(*Dest, 0, len * sizeof(wchar_t));
#ifdef __linux__
    size_t succeed = (size_t)-1;
    const char* fromCode = sizeof(wchar_t) == 2 ? "UTF-16" : "UTF-32";
    size_t f = len * sizeof(WCHAR_T), t = len * sizeof(wchar_t);
    iconv_t cd = iconv_open("UTF-32", fromCode);
    if (cd != (iconv_t)-1)
    {
        succeed = iconv(cd, (char**)&tmpShort, &f, (char**)&tmpWChar, &t);
        iconv_close(cd);
        if(succeed != (size_t)-1)
            return (uint32_t)succeed;
        ExtraLog("invalid encoding fron UTF-32"); // TUT toze nado !!
    }
#endif //__linux__
    for (; len; --len, ++res, ++tmpWChar, ++tmpShort)
    {
        *tmpWChar = (wchar_t)*tmpShort;
    }

    return res;
}
//---------------------------------------------------------------------------//
uint32_t getLenShortWcharStr(const WCHAR_T* Source)
{
    uint32_t res = 0;
    WCHAR_T *tmpShort = (WCHAR_T*)Source;

    while (*tmpShort++)
        ++res;

    return res;
}
//---------------------------------------------------------------------------//
#ifdef LINUX_OR_MACOS
WcharWrapper::WcharWrapper(const WCHAR_T* str) : m_str_WCHAR(NULL),
                           m_str_wchar(NULL)
{
    if (str)
    {
        int len = getLenShortWcharStr(str);
        m_str_WCHAR = new WCHAR_T[len + 1];
        memset(m_str_WCHAR,   0, sizeof(WCHAR_T) * (len + 1));
        memcpy(m_str_WCHAR, str, sizeof(WCHAR_T) * len);
        ::convFromShortWchar(&m_str_wchar, m_str_WCHAR);
    }
}
#endif
//---------------------------------------------------------------------------//
WcharWrapper::WcharWrapper(const wchar_t* str) :
#ifdef LINUX_OR_MACOS
    m_str_WCHAR(NULL),
#endif 
    m_str_wchar(NULL)
{
    if (str)
    {
        int len = wcslen(str);
        m_str_wchar = new wchar_t[len + 1];
        memset(m_str_wchar, 0, sizeof(wchar_t) * (len + 1));
        memcpy(m_str_wchar, str, sizeof(wchar_t) * len);
#ifdef LINUX_OR_MACOS
        ::convToShortWchar(&m_str_WCHAR, m_str_wchar);
#endif
    }

}
//---------------------------------------------------------------------------//
WcharWrapper::~WcharWrapper()
{
#ifdef LINUX_OR_MACOS
    if (m_str_WCHAR)
    {
        delete [] m_str_WCHAR;
        m_str_WCHAR = NULL;
    }
#endif
    if (m_str_wchar)
    {
        delete [] m_str_wchar;
        m_str_wchar = NULL;
    }
}
//---------------------------------------------------------------------------//
