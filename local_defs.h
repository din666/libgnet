#pragma once

#ifdef __linux__

#include <wctype.h>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
typedef boost::posix_time::ptime CTime;
typedef boost::posix_time::time_duration CTimeSpan;
#define _WT(text) L##text
#define _T(text) text

#endif
