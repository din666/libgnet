#include <iostream>

template<class T> void CHECK(const T & lhs, const T & rhs) {
    if ( lhs != rhs ) {
        std::cerr << "FAIL: '" << lhs << "' != '" << rhs << "'" << std::endl;
        //return;
    }
    //std::cerr << "OK";
}
