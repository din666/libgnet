CMAKE_MINIMUM_REQUIRED(VERSION 2.6 FATAL_ERROR)

set(PROJECT gnet)
project(${PROJECT})

link_directories(${CMAKE_CURRENT_SOURCE_DIR})

set( SOURCES
    grddongle.cpp
    Gnet.cpp
    dllmain.cpp
    stdafx.cpp
    stdafx.h
)

set(DEBUG_FLAGS "")
if (CMAKE_BUILD_TYPE EQUAL "DEBUG")
    set(DEBUG_FLAGS "-ggdb3 -O0")
endif()

#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -finput-charset=WINDOWS-1251 ${DEBUG_FLAGS} -Wall -pthread -march=i386 -m32 -fPIC")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -finput-charset=WINDOWS-1251 ${DEBUG_FLAGS} -Wall -pthread -fPIC")

if (EXTRALOGS)
    add_definitions(-DEXTRALOGS)
endif()

add_library(${PROJECT} ${SOURCES})
target_link_libraries( ${PROJECT} grdapi dl)
target_include_directories( ${PROJECT}
    PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}
)

# TEST
set( TEST_SOURCES
    test_gnet.cpp
)

add_executable(test_gnet ${TEST_SOURCES})
target_link_libraries(test_gnet gnet dl)
